package main

import (
	"net/http"
)

// getUserHandler handling /user endpoint
func (app *application) getUserHandler(w http.ResponseWriter, r *http.Request) {
	// Fetching the user with id 1
	user, _ := app.users.Get(1)
	app.logger.Print(user.Name)
}
