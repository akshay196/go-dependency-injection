package main

import "database/sql"

func getPort() int {
	return 4000
}

func getEnv() string {
	return "development"
}

func getDSN() string {
	return "user:Password@/dbname"
}

func getConfig() config {
	return config{
		Env: getEnv(),
		db: struct {
			dsn          string
			maxOpenConns int
		}{
			dsn:          getDSN(),
			maxOpenConns: 10,
		},
	}
}

func openDB(cfg config) *sql.DB {
	return &sql.DB{}
}
