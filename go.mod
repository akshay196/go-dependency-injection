module github.com/akshay196/go-dependency-injection

go 1.18

require (
	github.com/julienschmidt/httprouter v1.3.0
	gopkg.in/yaml.v3 v3.0.1
)
