package main

import (
	"log"
	"os"

	"github.com/akshay196/go-dependency-injection/models"
	"gopkg.in/yaml.v3"
)

type config struct {
	Env string
	db  struct {
		dsn          string
		maxOpenConns int
	}
}

type application struct {
	config        config
	configDecoder interface {
		Decode(interface{}) error
	}
	logger *log.Logger
	users  interface {
		Get(int) (*models.User, error)
	}
}

func main() {
	logger := log.New(os.Stdout, "", 0)

	file, err := os.Open("cfg.yaml")
	if err != nil {
		logger.Fatal(err)
	}
	// jsonDec := json.NewDecoder(file)
	yamlDec := yaml.NewDecoder(file)

	cfg := getConfig()
	db := openDB(cfg)

	app := &application{
		config:        cfg,
		configDecoder: yamlDec,
		logger:        logger,
		users:         &models.UserModel{DB: db},
	}

	err = app.serve()
	if err != nil {
		logger.Fatal(err)
	}
}
