package main

import (
	"log"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/akshay196/go-dependency-injection/mock"
)

func TestUserGet(t *testing.T) {
	cfg := getConfig()
	db := openDB(cfg)

	app := &application{
		logger: log.New(os.Stdout, "", 0),
		// mocked instead of real
		users: &mock.UserModel{DB: db},
	}

	ts := httptest.NewServer(app.routes())
	defer ts.Close()

	_, _ = ts.Client().Get(ts.URL + "/user")
}
