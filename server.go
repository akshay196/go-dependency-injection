package main

import (
	"net/http"
)

func (app *application) serve() error {
	addr := ":8667"
	srv := &http.Server{
		Addr:    addr,
		Handler: app.routes(),
	}
	var cfg config
	err := app.configDecoder.Decode(&cfg)
	if err != nil {
		return err
	}
	app.logger.Printf("Environment: %s", cfg.Env)
	app.logger.Printf("Starting server on %s", addr)
	return srv.ListenAndServe()
}
