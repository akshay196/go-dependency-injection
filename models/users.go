package models

import "database/sql"

type UserModel struct {
	DB *sql.DB
}

func (m *UserModel) Get(id int) (*User, error) {
	// Fetching real user from database ...
	return &User{
		ID:   id,
		Name: "Real User",
	}, nil
}
