package mock

import (
	"database/sql"

	"github.com/akshay196/go-dependency-injection/models"
)

type UserModel struct {
	DB *sql.DB
}

func (m *UserModel) Get(id int) (*models.User, error) {
	return &models.User{
		ID:   id,
		Name: "Mocked User",
	}, nil
}
